<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/master.css">
    <title>Examen</title>
  </head>
  <body>
    <h2>Añadir trabajador</h2>
    <form action="registro.php" method="post">
    <br><br>
    <label for="nombre">Nombre:</label>
    <br>
    <input type="text" name="nombre" id="nombre">
    <div id="hidden1" style="display:none;">No puede dejarse en blanco</div>
    <br><br>
    <label for="apellidos">Apellidos:</label>
    <br>
    <input type="text" name="apellidos" id="apellidos">
    <div id="hidden2" style="display:none;">No puede dejarse en blanco</div>
    <br><br>
    <label for="precio_hora">Precio por hora:</label>
    <br>
    <input type="text" name="precio_hora" id="precio_hora">
    <div id="hidden3" style="display:none;">No puede dejarse en blanco</div>
    <div id="hidden4" style="display:none;">Tiene que utilizar numeros</div>
    <br><br>
    <label for="horas">Horas:</label>
    <br>
    <input type="text" name="horas" id="horas">
    <div id="hidden3" style="display:none;">No puede dejarse en blanco</div>
    <div id="hidden4" style="display:none;">Tiene que utilizar numeros</div>
    <br><br>
    <input type="submit" value="Enviar" onclick="return comprobar()">
  </form>
    <script type="text/javascript" src="js/comprobar.js"></script>
  </body>
</html>
