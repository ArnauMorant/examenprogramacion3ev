<?php
class Trabajador extends Conexion
{
  private $nombre;
  private $apellidos;
  private $precio_hora;
  private $horas;
  function __construct(){
    $this->conectar();
  }
  public function comprobarCampos($post){
    $error=null;
    if(!isset($post)||!isset($post["nombre"])||!isset($post["apellidos"])||!isset($post["precio_hora"])||!isset($post["horas"])){
      $error="";
    }elseif($post["nombre"]==""){
      $error="No has introducido el nombre";
    }elseif($post["apellidos"]==""){
      $error="No has introducido los apellidos";
    }elseif($post["precio_hora"]==""){
      $error="No has introducido el precio por hora";
    }elseif($post["horas"]==""){
      $error="No has introducido las horas";
    }else{
      $error=false;
      $this->nombre=$post["nombre"];
      $this->apellidos=$post["apellidos"];
      $this->precio_hora=$post["precio_hora"];
      $this->horas=$post["horas"];
    }
    return $error;
  }
  public function registro(){
      $consulta="INSERT INTO trabajador (nombre, apellidos, precio_hora) VALUES ('$this->nombre', '$this->apellidos', $this->precio_hora)";
      $this->resultado=$this->conexion->query($consulta);
      $consulta2="INSERT INTO trabajador_proyecto (horas) VALUES ($this->horas)";
      $this->resultado=$this->conexion->query($consulta2);
  }
}
?>
