<?php
class Conexion
{
  public $conexion=null;
  private $host="localhost";
  private $user="root";
  private $pass="";
  private $db="proyectos";
  public function __construct(){
  }
  public function conectar(){
    $this->conexion = new mysqli($this->host, $this->user, $this->pass, $this->db);
    if ($this->conexion->connect_errno) {
      echo "Este invento no puede conectarse a MySQL :  (".$this->conexion->connect_errno.")".$this->conexion->connect_error;
    }
  }
}
?>
